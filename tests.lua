local algorithm = require('init')

do
  print('map')
  local mapped = algorithm.map(function(val) return 2 * val; end, {1, 2, 3});
  assert(#mapped == 3);
  assert(mapped[1] == 2);
  assert(mapped[2] == 4);
  assert(mapped[3] == 6);
end

do
  print('filter')
  local filtered = algorithm.filter(
    function(val) return math.fmod(val, 2) == 0; end, {1, 2, 3, 4}
  );
  assert(#filtered == 2);
  assert(filtered[1] == 2);
  assert(filtered[2] == 4);
end

do
  print('reduce')
  local result = algorithm.reduce(
    function(result, val) return result + val; end, {1, 2, 3}, 1
  );
  assert(result == 7);
end

do
  print('any_of')
  assert(algorithm.any_of(function(val) return val > 0; end, {-1, 1}));
  assert(algorithm.any_of(function(val) return val > 0; end, {-1, 0}) == false);
end

do
  print('all_of')
  assert(algorithm.all_of(function(val) return val > 0; end, {1, 2}));
  assert(algorithm.all_of(function(val) return val > 0; end, {0, 1}) == false);
end

do
  print('find')
  local val, idx =
    algorithm.find(function(val) return val > 0; end, {-1, 0, 1, 2});
  assert(val == 1);
  assert(idx == 3);
  local val, idx =
    algorithm.find(function(val) return val > 0; end, {-1, 0, 1, 2, 0}
                  ,algorithm.reverse_index_iterator);
  assert(val == 2);
  assert(idx == 4);
end
