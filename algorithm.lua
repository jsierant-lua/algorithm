local module = {};

function module.map(map_func, tab, iter)
  local mapped = {};
  for key,val in (iter or ipairs)(tab) do
    local mapped_value = map_func(val, key);
    if mapped_value then
      table.insert(mapped, mapped_value);
    end
  end
  return mapped;
end

function module.filter(accept_func, tab, iter_func)
  local filtered = {};

  local append = function(val) table.insert(filtered, val) end
  if iter_func == pairs then
    append = function(val, key) filtered[key] = val end
  end

  for key,val in (iter_func or pairs)(tab) do
    if accept_func(val, key) then
      append(val, key);
    end
  end

  return filtered;
end

function module.reduce(func, tab, init_value)
  local result = init_value;
  for key,val in pairs(tab) do
    result = func(result, val, key);
  end
  return result;
end

function module.any_of(pred, tab)
  return module.reduce(
    function(result, val, key)
      return result or pred(val, key);
    end
   ,tab
   ,false
  );
end

function module.all_of(pred, tab)
  return module.reduce(
    function(result, val, key)
      return result and pred(val, key);
    end
   ,tab
   ,true
  );
end

function module.find(pred, tab, iter)
  for key,val in (iter or pairs)(tab) do
    if pred(val, key) then
      return val,key;
    end
  end
  return nil;
end

function module.tab2string(tab)
  local result = {};
  for k,v in pairs(tab) do
    if type(v) == 'table' then
      v = string.format('{%s}', module.tab2string(v))
    end
    table.insert(result, string.format("%s=%s", k, v))
  end
  return string.format('{%s}', table.concat(result, ', '))
end

function module.reverse_index_iterator(tab)
  local index = #tab;
  return function ()
    index = index - 1;
    if index > 0 then
      return index,tab[index];
    end
  end
end

function module.element_iterator(tab)
  local index = 0;
  local count = #tab
  return function ()
    index = index + 1;
    if index <= count then
      return tab[index];
    end
  end
end

function module.remove(func, tab)
  local _, idx = module.find(func, tab);
  if idx then return table.remove(tab, idx); end
end

function module.not_(func)
  return function(...) return not func(...); end
end

function module.reverse(tab)
  if #tab == 0 then return tab; end
  for i=1,math.floor((#tab + 1)/2) do
    local tmp = tab[i];
    tab[i] = tab[#tab + 1 - i];
    tab[#tab + 1 - i] = tmp;
  end
  return tab;
end

function module.generate_range(start, stop, step)
  local result = {}
  for i=start,stop,(step or 1) do
    table.insert(result, i)
  end
  return result
end

function module.zip(tab1, tab2)
  local result = {}
  for i=1,#tab1 do table.insert(result, {tab1[i], tab2[i]}) end
  return result
end

function module.slice(tab, start, end_)
  local result = {}
  for i=start,(end_ or #tab) do table.insert(result, tab[i]) end
  return result
end

return module;
